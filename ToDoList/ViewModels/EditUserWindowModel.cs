﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using ToDoList.Models;
using ToDoList.Views;

namespace ToDoList.ViewModels
{
    public class EditUserWindowModel
    {
        private DispatcherTimer dispatcherTimer;
        public User User { get; set; }
        public Window window;

        private string oldLogin;
        private DatabaseContext context;
        private Label errorLabel;
        private AdminWindow previousWindow;

        public RelayCommand SaveChanges { get; set; }
        public EditUserWindowModel(DatabaseContext context, User user, Label errorLabel, AdminWindow previousWindow)
        {
            this.oldLogin = user.Login;
            this.User = user;
            this.context = context;
            this.errorLabel = errorLabel;
            this.previousWindow = previousWindow;

            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);

            SaveChanges = new RelayCommand(o => SaveDbChanges(), o => !string.IsNullOrWhiteSpace(User.Login) && !string.IsNullOrWhiteSpace(User.Password));
        }

        private void SaveDbChanges()
        {
            try
            {
                if (oldLogin != User.Login) //Login changed
                {
                    if (context.Users.Where(u => u.Login == User.Login).FirstOrDefault() != null)
                    {
                        errorLabel.Foreground = Brushes.Red;
                        errorLabel.Content = "User with this login already exists.";
                        errorLabel.Visibility = Visibility.Visible;
                        dispatcherTimer.Start();
                    }
                    else
                    {   
                        context.Users.Update(User);
                        errorLabel.Foreground = Brushes.Green;
                        errorLabel.Content = "Changes successfully changed.";
                        errorLabel.Visibility = Visibility.Visible;
                        dispatcherTimer.Start();
                    }
                }
                else
                {
                    context.Users.Update(User);
                    errorLabel.Foreground = Brushes.Green;
                    errorLabel.Content = "Changes successfully changed.";
                    errorLabel.Visibility = Visibility.Visible;
                    dispatcherTimer.Start();
                }

                context.SaveChanges();
                previousWindow.LoadUsers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            errorLabel.Visibility = Visibility.Collapsed;
            dispatcherTimer.IsEnabled = false;
        }
    }
}
