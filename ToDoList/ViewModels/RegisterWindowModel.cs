﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using ToDoList.Models;

namespace ToDoList.ViewModels
{
    public class RegisterWindowModel
    {
        public Window window;
        private DatabaseContext context;
        public string Nickname { get; set; }
        public string Name { get; set; } = "";
        public string Surname { get; set; } = "";

        private Label errorMessage;
        private DispatcherTimer dispatcherTimer;

        public RelayCommand RegisterCommand { get; set; }
        public RegisterWindowModel(DatabaseContext context, Label errorMessage)
        {
            this.context = context;
            this.errorMessage = errorMessage;

            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);

            RegisterCommand = new RelayCommand(o => Register(o), o => !string.IsNullOrWhiteSpace(Nickname) && !string.IsNullOrWhiteSpace((o as PasswordBox).Password));
        }

        private void Register(object parameter)
        {
            try
            {
                User user = context.Users.Where(u => u.Login.ToLower() == Nickname.ToLower()).FirstOrDefault();
                if (user != null)
                {
                    errorMessage.Content = "User with this nickname already exists.";
                    errorMessage.Visibility = Visibility.Visible;
                    dispatcherTimer.Start();
                }
                else
                {
                    user = new User();
                    user.Login = Nickname;
                    user.Name = Name;
                    user.Surname = Surname;
                    user.IsAdmin = false;
                    user.Password = (parameter as PasswordBox).Password;

                    context.Users.Add(user);
                    context.SaveChanges();
                    window.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            errorMessage.Visibility = Visibility.Collapsed;
            dispatcherTimer.IsEnabled = false;
        }
    }
}
