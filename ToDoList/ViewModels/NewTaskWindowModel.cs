﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ToDoList.Models;
using ToDoList.Views;

namespace ToDoList.ViewModels
{
    public class NewTaskWindowModel
    {
        public CustomTask Task { get; set; }

        private DatabaseContext context;
        public RelayCommand CreateTask { get; set; }
        public RelayCommand EditTask { get; set; }
        
        private object previousWindow;

        //When creating new task
        public NewTaskWindowModel(DatabaseContext context, User user, Button btn, object previousWindow, NewTaskWindow currentWindow)
        {
            try
            {
                this.context = context;
                this.previousWindow = previousWindow;
                Task = new CustomTask();
                btn.IsEnabled = false;
                btn.Opacity = 0.5;

                CreateTask = new RelayCommand(o => AddTask(user, ref previousWindow, currentWindow), o => !string.IsNullOrWhiteSpace(Task.Name));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //When editing a task
        public NewTaskWindowModel(DatabaseContext context, CustomTask task, Button btn, object previousWindow, NewTaskWindow currentWindow)
        {
            try
            {
                this.context = context;
                this.previousWindow = previousWindow;
                this.Task = task;
                btn.IsEnabled = false;
                btn.Opacity = 0.5;

                EditTask = new RelayCommand(o => UpdateTask(ref previousWindow,task.User, currentWindow), o => !string.IsNullOrWhiteSpace(Task.Name));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddTask<T>(User user, ref T previousWindow, NewTaskWindow currentWindow)
        {
            try
            {
                Task.User = user;
                context.Tasks.Add(Task);
                context.SaveChanges();
                switch(previousWindow.GetType().Name)
                {
                    case "TasksWindow":
                        (previousWindow as TasksWindow).LoadTasks();
                        break;
                    case "AdminWindow":
                        (previousWindow as AdminWindow).LoadTasks();
                        break;
                    default:
                        MessageBox.Show("An error occurred.");
                        break;
                }
                currentWindow.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateTask<T>(ref T previousWindow,User user ,NewTaskWindow currentWindow)
        {
            try
            {
                context.Tasks.Update(Task);
                context.SaveChanges();
                switch (previousWindow.GetType().Name)
                {
                    case "TasksWindow":
                        (previousWindow as TasksWindow).LoadTasks();
                        break;
                    case "EditUserWindow":
                        (previousWindow as EditUserWindow).LoadTasks(user);
                        break;
                    case "AdminWindow":
                        (previousWindow as AdminWindow).LoadTasks();
                        break;
                    default:
                        MessageBox.Show("An error occurred.");
                        break;
                }
                currentWindow.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
