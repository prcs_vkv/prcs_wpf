﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using ToDoList.Models;
using ToDoList.Views;

namespace ToDoList.ViewModels
{
    public class LoginWindowModel
    {
        public Window window;

        private DatabaseContext context;
        private DispatcherTimer dispatcherTimer;
        public string Login { get; set; }
        public RelayCommand LoginCommand { get; set; }
        public RelayCommand RegisterCommand { get; set; }

        private System.Windows.Controls.Label errorMessage;

        public LoginWindowModel(DatabaseContext context, System.Windows.Controls.Label errorMessage)
        {
            this.context = context;
            this.errorMessage = errorMessage;

            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);

            LoginCommand = new RelayCommand(o => CheckLogin(o), o => !string.IsNullOrWhiteSpace(Login) && !string.IsNullOrWhiteSpace((o as PasswordBox).Password));
            RegisterCommand = new RelayCommand(o => OpenRegisterWindow(), o => true);
        }


        private void CheckLogin(object parameter)
        {
            try
            {
                var passwordBox = parameter as PasswordBox;
                var password = passwordBox.Password;

                User user = context.Users.Where(u => u.Login == Login).FirstOrDefault();

                if (user == null || user.Password != password)
                {
                    errorMessage.Content = "Incorrect login!";
                    errorMessage.Visibility = Visibility.Visible;
                    dispatcherTimer.Start();
                    return;
                }

                if (user.IsAdmin)
                {
                    AdminWindow adminWindow = new AdminWindow(user, context);
                    window.Close();
                    adminWindow.ShowDialog();
                    
                }
                else
                {
                    TasksWindow tasksWindow = new TasksWindow(user, context);
                    window.Close();
                    tasksWindow.ShowDialog();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenRegisterWindow()
        {
            RegisterWindow registerWindow = new RegisterWindow(context);
            registerWindow.ShowDialog();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            errorMessage.Visibility = Visibility.Collapsed;
            dispatcherTimer.IsEnabled = false;
        }
    }
}
