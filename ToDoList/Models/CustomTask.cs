﻿namespace ToDoList.Models
{
    public class CustomTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; } = "";
        public bool IsDone { get; set; } = false;
        public User User { get; set; }

    }
}