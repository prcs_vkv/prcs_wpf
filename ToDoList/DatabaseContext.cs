﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Models;

namespace ToDoList
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<CustomTask> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder model)
        {
            model.Entity<User>().HasKey(u => u.Id);
            model.Entity<CustomTask>().HasKey(ct => ct.Id);

            model.Entity<CustomTask>().HasOne(ct => ct.User).WithMany(u => u.Tasks);
            model.Entity<CustomTask>().Property(ct => ct.Id).ValueGeneratedOnAdd();
            model.Entity<User>().Property(u => u.Id).ValueGeneratedOnAdd();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=AppSQlite.db");
        }
    }
}
