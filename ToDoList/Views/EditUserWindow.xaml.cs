﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDoList.Models;
using ToDoList.ViewModels;

namespace ToDoList.Views
{
    /// <summary>
    /// Interakční logika pro EditUserWindow.xaml
    /// </summary>
    public partial class EditUserWindow : Window
    {
        private DatabaseContext context;
        public EditUserWindow(DatabaseContext context, User user, AdminWindow previousWindow)
        {
            InitializeComponent();
            this.context = context;
            EditUserWindowModel editUserModel = new EditUserWindowModel(context, user, errorLabel, previousWindow);
            editUserModel.window = this;
            this.DataContext = editUserModel;

            LoadTasks(user);
        }

        public void LoadTasks(User user)
        {
            sv.Children.Clear();
            sv.RowDefinitions.Clear();
            int i = 0;
            foreach (CustomTask task in context.Tasks.Where(t => t.User == user))
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(45);
                sv.RowDefinitions.Add(rowDefinition);

                Label nameLabel = new Label();
                nameLabel.Name = "text" + task.Id;
                nameLabel.Content = task.Name;
                nameLabel.FontSize = 18;
                nameLabel.Width = 180;
                nameLabel.IsEnabled = true;
                nameLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
                nameLabel.HorizontalAlignment = HorizontalAlignment.Center;
                nameLabel.VerticalAlignment = VerticalAlignment.Top;
                Grid.SetRow(nameLabel, i);
                Grid.SetColumn(nameLabel, 0);
                sv.Children.Add(nameLabel);

                CheckBox state = new CheckBox();
                state.Name = "state" + task.Id;
                state.FontSize = 20;
                state.HorizontalAlignment = HorizontalAlignment.Center;
                state.VerticalAlignment = VerticalAlignment.Top;
                state.Click += ChangeState;
                state.LayoutTransform = new ScaleTransform(1.5, 1.5);
                if (task.IsDone) state.IsChecked = true;
                else state.IsChecked = false;
                Grid.SetRow(state, i);
                Grid.SetColumn(state, 1);
                sv.Children.Add(state);

                Button detail = new Button();
                detail.Name = "detail" + task.Id;
                detail.Content = "Detail";
                detail.FontSize = 18;
                detail.Width = 80;
                detail.HorizontalAlignment = HorizontalAlignment.Center;
                detail.VerticalAlignment = VerticalAlignment.Top;
                detail.Click += EditTask;
                Grid.SetRow(detail, i);
                Grid.SetColumn(detail, 2);
                sv.Children.Add(detail);

                Button delete = new Button();
                delete.Name = "delete" + task.Id;
                delete.Content = "Remove";
                delete.FontSize = 18;
                delete.Width = 80;
                delete.HorizontalAlignment = HorizontalAlignment.Center;
                delete.VerticalAlignment = VerticalAlignment.Top;
                delete.Click += RemoveTask;
                Grid.SetRow(delete, i);
                Grid.SetColumn(delete, 3);
                sv.Children.Add(delete);

                i++;              
            }
        }

        private void EditTask(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                int taskId = int.Parse(btn.Name.Substring(6));
                CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
                NewTaskWindow newTaskWindow = new NewTaskWindow(context, task, this);
                newTaskWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RemoveTask(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                int taskId = int.Parse(btn.Name.Substring(6));
                CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
                context.Tasks.Remove(task);
                context.SaveChanges();

                sv.Children.Clear();
                LoadTasks(task.User);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ChangeState(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox state = (CheckBox)sender;
                int taskId = int.Parse(state.Name.Substring(5));
                CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
                if (state.IsChecked == true)
                    task.IsDone = true;
                else
                    task.IsDone = false;

                context.SaveChanges();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
