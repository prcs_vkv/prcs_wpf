﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDoList.Models;
using ToDoList.ViewModels;

namespace ToDoList.Views
{
    /// <summary>
    /// Interakční logika pro NewTaskWindow.xaml
    /// </summary>
    public partial class NewTaskWindow : Window
    {

        //When user is creating task
        public NewTaskWindow(DatabaseContext context, User user, object previousWindow)
        {
            InitializeComponent();
            NewTaskWindowModel newTaskWindowModel = new NewTaskWindowModel(context, user, editBtn, previousWindow, this);
            this.DataContext = newTaskWindowModel;
        }

        //When user is editing task
        public NewTaskWindow(DatabaseContext context, CustomTask task, object previousWindow)
        {
            InitializeComponent();
            NewTaskWindowModel newTaskWindowModel = new NewTaskWindowModel(context, task, createBtn, previousWindow, this);
            this.DataContext = newTaskWindowModel;
        }
    }
}
