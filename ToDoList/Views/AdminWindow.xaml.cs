﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDoList.Models;

namespace ToDoList.Views
{
    /// <summary>
    /// Interakční logika pro AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private DatabaseContext context;
        private User loggedUser;

        public AdminWindow(User loggedUser, DatabaseContext context)
        {
            InitializeComponent();

            groupBox.Header = "Users";
            this.loggedUser = loggedUser;
            this.context = context;
            if (loggedUser.Name != "" && loggedUser.Surname != "")
                usernameTxtBox.Content = loggedUser.Name + " " + loggedUser.Surname + " (" + loggedUser.Login + ")";
            else 
                usernameTxtBox.Content = loggedUser.Login;
            LoadUsers();
          
        }

        public void LoadUsers()
        {
            sv.Children.Clear();
            sv.RowDefinitions.Clear();
            int i = 0;
            foreach (User user in context.Users.Where(u => u.Login != loggedUser.Login))
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(45);
                sv.RowDefinitions.Add(rowDefinition);

                Label userLoginLabel = new Label();
                userLoginLabel.Name = user.Login;
                userLoginLabel.Content = user.Login;
                userLoginLabel.FontSize = 20;
                userLoginLabel.Width = 225;
                userLoginLabel.IsEnabled = false;
                userLoginLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
                userLoginLabel.HorizontalAlignment = HorizontalAlignment.Center;
                userLoginLabel.VerticalAlignment = VerticalAlignment.Top;
                Grid.SetRow(userLoginLabel, i);
                Grid.SetColumn(userLoginLabel, 0);
                sv.Children.Add(userLoginLabel);

                CheckBox isAdmin = new CheckBox();
                isAdmin.Name = "admin" + user.Id;
                isAdmin.FontSize = 20;
                isAdmin.IsEnabled = false;
                isAdmin.LayoutTransform = new ScaleTransform(2.5, 2.5);
                if (user.IsAdmin) isAdmin.IsChecked = true;
                else isAdmin.IsChecked = false;
                isAdmin.HorizontalContentAlignment = HorizontalAlignment.Center;
                isAdmin.HorizontalAlignment = HorizontalAlignment.Center;
                isAdmin.VerticalAlignment = VerticalAlignment.Top;
                Grid.SetRow(isAdmin, i);
                Grid.SetColumn(isAdmin, 1);
                sv.Children.Add(isAdmin);

                Button detail = new Button();
                detail.Name = user.Login;
                detail.Content = "Detail";
                detail.FontSize = 20;
                detail.Width = 100;
                detail.HorizontalAlignment = HorizontalAlignment.Center;
                detail.VerticalAlignment = VerticalAlignment.Top;
                detail.Click += EditUser;
                Grid.SetRow(detail, i);
                Grid.SetColumn(detail, 2);
                sv.Children.Add(detail);

                Button delete = new Button();
                delete.Name = user.Login;
                delete.Content = "Remove";
                delete.FontSize = 20;
                delete.Width = 100;
                delete.HorizontalAlignment = HorizontalAlignment.Center;
                delete.VerticalAlignment = VerticalAlignment.Top;
                delete.Click += RemoveUser;
                Grid.SetRow(delete, i);
                Grid.SetColumn(delete, 3);
                sv.Children.Add(delete);

                i++;
            }
        }
        private void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(context);
            this.Close();
            mainWindow.ShowDialog();
        }

        private void EditUser(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            EditUserWindow editUserWindow = new EditUserWindow(context, context.Users.Where(u => u.Login == btn.Name).FirstOrDefault(), this);
            editUserWindow.ShowDialog();
        }

        private void RemoveUser(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                if (btn.Name == "admin")
                {
                    MessageBox.Show("Tohoto uživatele nelze smazat!");
                }
                else
                {
                    context.Users.Remove(context.Users.Where(u => u.Login == btn.Name).FirstOrDefault());
                    context.SaveChanges();
                    LoadUsers();
                }          
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ChangeView(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Content.ToString() == "My tasks")
            {
                btn.Content = "Users";
                headerLabel1.Content = "Task";
                headerLabel2.Content = "Is done";
                groupBox.Header = "Tasks";
                newTaskBtn.Visibility = Visibility.Visible;
                LoadTasks();
                return;
            }
            if (btn.Content.ToString() == "Users")
            {
                btn.Content = "My tasks";
                headerLabel1.Content = "Login";
                headerLabel2.Content = "Is admin";
                groupBox.Header = "Users";
                newTaskBtn.Visibility = Visibility.Hidden;
                LoadUsers();
                return;
            }
            else
            {
                MessageBox.Show("Something's wrong, contact IT department");
            }
        }

        public void LoadTasks()
        {
            sv.Children.Clear();
            sv.RowDefinitions.Clear();
            int i = 0;
            foreach (CustomTask task in context.Tasks.Where(t => t.User == loggedUser))
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(45);
                sv.RowDefinitions.Add(rowDefinition);

                Label nameLabel = new Label();
                nameLabel.Name = "label" + task.Id;
                nameLabel.Content = task.Name;
                nameLabel.FontSize = 20;
                nameLabel.Width = 225;
                nameLabel.IsEnabled = false;
                nameLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
                nameLabel.HorizontalAlignment = HorizontalAlignment.Center;
                nameLabel.VerticalAlignment = VerticalAlignment.Top;
                Grid.SetRow(nameLabel, i);
                Grid.SetColumn(nameLabel, 0);
                sv.Children.Add(nameLabel);

                CheckBox state = new CheckBox();
                state.Name = "state" + task.Id;
                state.FontSize = 20;
                state.HorizontalAlignment = HorizontalAlignment.Center;
                state.VerticalAlignment = VerticalAlignment.Top;
                state.Click += ChangeState;
                state.LayoutTransform = new ScaleTransform(2.5, 2.5);
                if (task.IsDone) state.IsChecked = true;
                else state.IsChecked = false;
                Grid.SetRow(state, i);
                Grid.SetColumn(state, 1);
                sv.Children.Add(state);

                Button detail = new Button();
                detail.Name = "detail" + task.Id;
                detail.Content = "Detail";
                detail.FontSize = 20;
                detail.Width = 100;
                detail.HorizontalAlignment = HorizontalAlignment.Center;
                detail.VerticalAlignment = VerticalAlignment.Top;
                detail.Click += EditTask;
                Grid.SetRow(detail, i);
                Grid.SetColumn(detail, 2);
                sv.Children.Add(detail);

                Button delete = new Button();
                delete.Name = "delete" + task.Id;
                delete.Content = "Remove";
                delete.FontSize = 20;
                delete.Width = 100;
                delete.HorizontalAlignment = HorizontalAlignment.Center;
                delete.VerticalAlignment = VerticalAlignment.Top;
                delete.Click += RemoveTask;
                Grid.SetRow(delete, i);
                Grid.SetColumn(delete, 3);
                sv.Children.Add(delete);

                i++;
            }
        }

        private void EditTask(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int taskId = int.Parse(btn.Name.Substring(6));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            NewTaskWindow newTaskWindow = new NewTaskWindow(context, task, this);
            newTaskWindow.ShowDialog();
        }

        private void RemoveTask(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int taskId = int.Parse(btn.Name.Substring(6));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            context.Tasks.Remove(task);
            context.SaveChanges();

            sv.Children.Clear();
            LoadTasks();

        }

        private void ChangeState(object sender, RoutedEventArgs e)
        {
            CheckBox state = (CheckBox)sender;
            int taskId = int.Parse(state.Name.Substring(5));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            if (state.IsChecked == true)
                task.IsDone = true;
            else
                task.IsDone = false;

            context.SaveChanges();
        }

        private void CreateTask(object sender, RoutedEventArgs e)
        {
            NewTaskWindow newTaskWindow = new NewTaskWindow(context, loggedUser, this);
            newTaskWindow.ShowDialog();
        }
    }
}
