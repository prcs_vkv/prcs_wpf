﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToDoList.Models;
using ToDoList.ViewModels;

namespace ToDoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {  
        public MainWindow()
        {
            InitializeComponent();
            
            try
            {
                DatabaseContext context = new DatabaseContext();
                context.Database.EnsureCreated();

                LoginWindowModel loginWindowModel = new LoginWindowModel(context, errorMessage);
                loginWindowModel.window = this;
                this.DataContext = loginWindowModel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public MainWindow(DatabaseContext context)
        {
            InitializeComponent();

            try
            {
                LoginWindowModel loginWindowModel = new LoginWindowModel(context, errorMessage);
                loginWindowModel.window = this;
                this.DataContext = loginWindowModel;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
