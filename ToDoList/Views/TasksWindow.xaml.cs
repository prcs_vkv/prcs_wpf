﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDoList.ViewModels;
using ToDoList.Models;

namespace ToDoList.Views
{
    /// <summary>
    /// Interakční logika pro TasksWindow.xaml
    /// </summary>
    public partial class TasksWindow : Window
    {
        private DatabaseContext context;
        private User user;
        public TasksWindow(User user, DatabaseContext context)
        {         
            InitializeComponent();
            this.context = context;
            this.user = user;
            if (user.Name != "" && user.Surname != "")
                usernameTxtBox.Content = user.Name + " " + user.Surname + " (" + user.Login + ")";
            else usernameTxtBox.Content = user.Login;
            
            LoadTasks();           
        }

        public void LoadTasks()
        {
            sv.Children.Clear();
            sv.RowDefinitions.Clear();
            int i = 0;
            foreach (CustomTask task in context.Tasks.Where(t => t.User == user))
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(45);
                sv.RowDefinitions.Add(rowDefinition);

                Label nameLabel = new Label();
                nameLabel.Name = "label" + task.Id;
                nameLabel.Content = task.Name;
                nameLabel.FontSize = 20;
                nameLabel.Width = 225;
                nameLabel.IsEnabled = false;
                nameLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
                nameLabel.HorizontalAlignment = HorizontalAlignment.Center;
                nameLabel.VerticalAlignment = VerticalAlignment.Top;
                Grid.SetRow(nameLabel, i);
                Grid.SetColumn(nameLabel, 0);
                sv.Children.Add(nameLabel);

                CheckBox state = new CheckBox();
                state.Name = "state" + task.Id;
                state.FontSize = 20;
                state.HorizontalAlignment = HorizontalAlignment.Center;
                state.VerticalAlignment = VerticalAlignment.Top;
                state.Click += ChangeState;
                state.LayoutTransform = new ScaleTransform(2.5, 2.5);
                if (task.IsDone) state.IsChecked = true;
                else state.IsChecked = false;
                Grid.SetRow(state, i);
                Grid.SetColumn(state, 1);
                sv.Children.Add(state);

                Button detail = new Button();
                detail.Name = "detail" + task.Id;
                detail.Content = "Detail";
                detail.FontSize = 20;
                detail.Width = 100;
                detail.HorizontalAlignment = HorizontalAlignment.Center;
                detail.VerticalAlignment = VerticalAlignment.Top;
                detail.Click += EditTask;
                Grid.SetRow(detail, i);
                Grid.SetColumn(detail, 2);
                sv.Children.Add(detail);

                Button delete = new Button();
                delete.Name = "delete" + task.Id;
                delete.Content = "Remove";
                delete.FontSize = 20;
                delete.Width = 100;
                delete.HorizontalAlignment = HorizontalAlignment.Center;
                delete.VerticalAlignment = VerticalAlignment.Top;
                delete.Click += RemoveTask;
                Grid.SetRow(delete, i);
                Grid.SetColumn(delete, 3);
                sv.Children.Add(delete);

                i++;
            }
        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(context);
            this.Close();
            mainWindow.ShowDialog();
        }

        private void CreateTask(object sender, RoutedEventArgs e)
        {
            NewTaskWindow newTaskWindow = new NewTaskWindow(context, user, this);
            newTaskWindow.ShowDialog();
        }
        private void EditTask(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int taskId = int.Parse(btn.Name.Substring(6));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            NewTaskWindow newTaskWindow = new NewTaskWindow(context, task, this);
            newTaskWindow.ShowDialog();
        }

        private void RemoveTask(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int taskId = int.Parse(btn.Name.Substring(6));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            context.Tasks.Remove(task);
            context.SaveChanges();

            sv.Children.Clear();
            LoadTasks(); 
        }

        private void ChangeState(object sender, RoutedEventArgs e)
        {
            CheckBox state = (CheckBox)sender;
            int taskId = int.Parse(state.Name.Substring(5));
            CustomTask task = context.Tasks.Where(t => t.Id == taskId).FirstOrDefault();
            if (state.IsChecked == true)
                task.IsDone = true;
            else
                task.IsDone = false;

            context.SaveChanges();
        }
    }
}
